// MCS.cpp, Congyingzi Zhang
//
// This program follows the generic algorithm of MCS used for evaluating system reliablity. 
// The program includes the following steps:
// 1. Begin MCS with reading data from a file
// 2. Sample a single state
// 3. Classify sampled state
// 4. Perform calculation
// 5. Check if converged
// 6. End when converged.
//
// Small test-bed CCS used in initial simulation
// 			P(#cores)	M(GB)	H(GB)	B(Mbps)
// Availble	400			400		10000	50000
// Each VM	2			2		100		100
// 
// Write a class mcssim, so to seperate VM & Server setting apart from the simulator 

#include <iostream>
#include <math.h>  
#include <ctime>
#include <iomanip>
#include <stdlib.h>
#include <fstream>
#include "CStopWatch.h"
using namespace std;

// Constant
// const double NANOSECONDS_TO_MILISECONDS = 1.0/1000000.0;
const double AFR_P = 0.02;
const double AFR_M = 0.01;
const double AFR_H = 0.08;
const double AFR_B = 0.01;
// Unit of different type of sources on each device
const int VM_P = 2, VM_M = 2, VM_H = 100, VM_B = 100; 

CStopWatch totaltime;
const int vm_requested = 98;
const int server_num = 100;

// For allowing grey state, allowing certain type of device having multiple seperate resource on one server
const int DEVICE_P =4, DEVICE_M =4, DEVICE_H =4, DEVICE_B = 4;

// UNIT * DEVICE = TOTAL RESOURCE UNIT PER SERVER
// 100 server, 
const int UNIT_P = 1 , UNIT_M = 1, UNIT_H = 25, UNIT_B = 250; 

// 50 server
// const int UNIT_P = 8, UNIT_M = 8, UNIT_H = 250, UNIT_B = 1000;

// 10 server
 //const int UNIT_P = 10, UNIT_M = 10, UNIT_H = 250, UNIT_B = 1250;

// 1 server
//const int UNIT_P = 400, UNIT_M = 400, UNIT_H = 1000, UNIT_B = 50000;


// Function prototypes
bool bit_gene(double afri);
bool iteration_cond(int iteration_counter, double reliable, double sigma, double& time, ostream& outfile);
bool **sample_gene(int server_num, int device);
int cal_available(bool **sample, int server_num, int device_case);
int cal_curtailed(int available, int request);
void memory_release(bool **sample);
void find_AFR_DN(int device_case, double& afri, int& unit_num, int& device_num);
double cal_utili(const int& required, const int& available);

int main(int argc, char* argv[]) 

{
	ofstream outfile;
	//to continuously record:
	//outfile.open("data.txt",std::ofstream::out | std::ofstream::app);
	outfile.open("MSdata.txt");
	srand(time(NULL));
	//if (argc != 3) return -1;
	//int vm_requested = atoi(argv[1]);
	int requested_P = vm_requested * VM_P, 
		requested_M = vm_requested * VM_M, 
		requested_H = vm_requested * VM_H, 
		requested_B = vm_requested * VM_B;
		//server_num = atoi(argv[2]);	

	int available_P=0, available_M=0, available_H=0, available_B=0,
	curtailed_P=0,curtailed_M=0, curtailed_H=0, curtailed_B=0;	
	
	double utili_P=0, utili_M=0, utili_H=0,utili_B=0;
	double acumu_utili_P=0, acumu_utili_M=0, acumu_utili_H=0, acumu_utili_B=0;
	double ADU_P=0, ADU_H=0, ADU_M=0, ADU_B=0;
	
	int iteration_counter = 0;
	double reliable=0, sigma2=0, sigma=0, failure_rate=0;
	int Sx=0, Sk = 0, failure = 0;
	double time = 0.0;
	
	// Loop through iterations:
	// at the the condition of (iterating criteria)
	// Iterating criteria:
	// 	iterations > 10 and 0 < sigma < 0.08
	//	iterations > 20000 and R > 0.999999
	// 1. Generate a sample for from one iteration bool sample[server_num][unit_num]
	// 2. Calculate number of certain device which are available among all server
	// 3. Compare P/M/H/B-availablel with P/M/H/B-requested to get P/M/H/B-curtailed
	// 4. Calculate Sx by adding up curtailed resources. 0 for non-functional system single state
	//    1 for functional system single state
	// 5. Update total success state counter Sk, calculate reliable and sigma	
	
	// Start timer
	totaltime.startTimer();

	do
	{
		// Starting a new iteration, update iteration_counter
		iteration_counter++;
		
		// Collect sampling result from sample[server_num][UNIT_P/M/H/B]
		bool **sample_P = sample_gene(server_num, 0),
			 **sample_M = sample_gene(server_num, 1),
			 **sample_H = sample_gene(server_num, 2),
			 **sample_B = sample_gene(server_num, 3);
		

		// Calculate number of certain device which are available among all server
		available_P = cal_available(sample_P, server_num, 0);
		available_M = cal_available(sample_M, server_num, 1);
		available_H = cal_available(sample_H, server_num, 2);
		available_B = cal_available(sample_B, server_num, 3);
		// cout << "available_P=" << available_P << " ";
		// cout << "available_M=" << available_M << " ";
		// cout << "available_H=" << available_H << " ";
		// cout << "available_B=" << available_B << " ";

		// Calculate P/M/H/B-curtailed for all devices, return 0 for not curtailed, 1 for curtailed
		curtailed_P = cal_curtailed(available_P, requested_P);
		curtailed_M = cal_curtailed(available_M, requested_M);
		curtailed_H = cal_curtailed(available_H, requested_H);
		curtailed_B = cal_curtailed(available_B, requested_B);
		// cout << "diff_P=" << available_P-requested_P << " ";
		// cout << "diff_M=" << available_M-requested_M << " ";
		// cout << "diff_H=" << available_H-requested_H << " ";
		// cout << "diff_B=" << available_B-requested_B << " ";	
	
		// // Calculate P/M/H/B-avgDeviceUtili
		// cout << fixed << setprecision(2) << "ADU_P=" << cal_avgDeviceUtili(requested_P, available_P, acumu_utili_P, iteration_counter)*100  << "% ";
		// cout << fixed << setprecision(2) << "ADU_M=" << cal_avgDeviceUtili(requested_M, available_M, acumu_utili_M, iteration_counter)*100  << "% ";
		// cout << fixed << setprecision(2) << "ADU_H=" << cal_avgDeviceUtili(requested_H, available_H, acumu_utili_H, iteration_counter)*100  << "% ";
		// cout << fixed << setprecision(2) << "ADU_B=" << cal_avgDeviceUtili(requested_B, available_B, acumu_utili_B, iteration_counter)*100  << "% ";
		

		
		// Calculate Sx for the system state in one sample		
		if(curtailed_P+curtailed_M+curtailed_H+curtailed_B > 0)
		{
			Sx = 0; 
			failure++;
		}
			
		else if(curtailed_P+curtailed_M+curtailed_H+curtailed_B == 0)
			Sx = 1;
		
		Sk += Sx; // Success
		
		//// Calculate utilization
		//utili_P = cal_utili(requested_P, available_P);
		//utili_M = cal_utili(requested_M, available_M);
		//utili_H = cal_utili(requested_H, available_H);
		//utili_B = cal_utili(requested_B, available_B);
		//// cout << utili_P << " " << utili_M << " " << utili_H << " " << utili_B << " ";
		//
		//acumu_utili_P += utili_P;
		//acumu_utili_M += utili_M;
		//acumu_utili_H += utili_H;
		//acumu_utili_B += utili_B;	
		//// cout << acumu_utili_P << " " << acumu_utili_M << " " << acumu_utili_H << " " << acumu_utili_B << " ";
		//
		//ADU_P = acumu_utili_P*100.0/Sk;
		//ADU_M = acumu_utili_M*100.0/Sk;
		//ADU_H = acumu_utili_H*100.0/Sk;
		//ADU_B = acumu_utili_B*100.0/Sk; 
		//cout << fixed << setprecision(2) << "ADU_P=" << ADU_P  << "% ";
		//cout << fixed << setprecision(2) << "ADU_M=" << ADU_M  << "% ";
		//cout << fixed << setprecision(2) << "ADU_H=" << ADU_H  << "% ";
		//cout << fixed << setprecision(2) << "ADU_B=" << ADU_B  << "% ";		
		
		outfile << "Itr[" << iteration_counter << "] ";
		outfile << "Failure[" << failure << "] "; 

		//cout << "Iteration=" << iteration_counter << " ";
		//cout << "Failure=" << failure << " "; 

		// Dertermine Convergence
		
		reliable = (double)Sk/iteration_counter; 	
		outfile << fixed << setprecision(4) << "R=" << reliable << " ";
		//cout << fixed << setprecision(4) << "R=" << reliable << " ";
		
		failure_rate = 1- reliable; 
		outfile << fixed << setprecision(4) << "F=" << failure_rate << " ";
		//cout << fixed << setprecision(4) << "F=" << failure_rate << " ";
		
		sigma2 = (failure_rate - pow(failure_rate,2.0))/iteration_counter;
		
		sigma = (sqrt(sigma2))/failure_rate; 
		outfile << fixed << setprecision(4) << "sigma=" << sigma  << " " << endl;
		//cout << fixed << setprecision(4) << "sigma=" << sigma  << " " << endl;

		// Once all the calculations are finished, release the allocated memory
		memory_release(sample_P);
		memory_release(sample_M);
		memory_release(sample_H);
		memory_release(sample_B);	
		
	}while(iteration_cond(iteration_counter, reliable, sigma, time, outfile));
	
	totaltime.stopTimer();
	// outfile << fixed << setprecision(4) << ADU_P << " " << ADU_M << " " << ADU_H << " " << ADU_B << " ";
	outfile << vm_requested << "VM requested (P=" << VM_P << ", M=" << VM_M << ", H=" << VM_H << ", B=" << VM_B <<")" << endl; 
	outfile << server_num << "PM available" << endl; 
	outfile << "TOTAL DEVICE RESOURCE = DEVICE NUMBER * UNIT PER DEVICE" << endl;
	outfile << "P: " << DEVICE_P * UNIT_P <<"="<< DEVICE_P << " * "<< UNIT_P << endl; 
	outfile << "M: " << DEVICE_M * UNIT_M <<"="<< DEVICE_M << " * "<< UNIT_M << endl; 
	outfile << "H: " << DEVICE_H * UNIT_H <<"="<< DEVICE_H << " * "<< UNIT_H << endl; 
	outfile << "B: " << DEVICE_B * UNIT_B <<"="<< DEVICE_B << " * "<< UNIT_B << endl; 
	outfile << "Total Itr:"<< iteration_counter << " Total Filure:" << failure << " R:" 
		<< reliable << " Time(ms):" <<  fixed << setprecision(4) << time << endl;

	//outfile << fixed << setprecision(4) << ADU_P << " " << ADU_M << " " << ADU_H << " " << ADU_B << " ";
	//outfile << iteration_counter << " " << failure << " " << reliable << " " <<  fixed << setprecision(4) << time << endl;
	outfile.close();
  return 0;
}
	

// Sampling:
bool bit_gene(double afri)
{
	
	double ui = rand()/double (RAND_MAX); // ui [0,1]
	double ri = -log( 1 - ui )/afri; // ri exponentially distributed random #
	
	if (ri <= afri)
		return 0;
	else
		return 1;
}

bool **sample_gene(int server_num, int device_case)
{
	// Declare afri, unit_num, and device_num
	double afri;
	int unit_num, device_num;
	// unit_num: amount of resource on each type of device on one server,
	//	for examle, unit_num for HHD = 100GB 
	// device_num: number of subparts that one type of device can seperate
	// into on one server, for example, device_num = 4 for CPU when grey state
	// allows one core down and the other three on for one server 
	find_AFR_DN(device_case, afri, unit_num, device_num);
	
	// Memory allocation for a bool dynamic 2d array sample[server_num][unit_num]
	bool **sample;
	
	sample = new bool*[server_num];
	for(int i = 0; i < server_num; ++i)
		sample[i] = new bool [device_num];
		
	
	// Sampling 
	for(int s = 0; s < server_num; s++)// Fill the rows
	{
		for(int d = 0; d < device_num; d++)// Fill the cols
		{
			sample[s][d] = bit_gene(afri); // Access each element through the sample pointer
		}
	}
	return sample;
}

// Classification:
// Return number of certain device that are available in one sample among all servers
int cal_available(bool **sample, int server_num, int device_case)
{
	// Define afri and unit_num
	double afri;
	int unit_num, device_num;
	find_AFR_DN(device_case, afri, unit_num, device_num);
	
	int available_device = 0;
	for(int i = 0; i < server_num; i++)
	{
		for(int j = 0; j < device_num; j++)
		{
			if(sample[i][j])
				available_device++;
		}
	}
	return (available_device * unit_num); // Total available unit of certain device on all servers
}

// Return evaluation of a curtailed device, return 0 for not curtailed, 1 for curtailed
int cal_curtailed(int available, int request)
{
	if( request <= available)
		return 0;
	else 
		return 1;
}

// Determine convergence
bool iteration_cond(int iteration_counter, double reliable, double sigma, double& time, ostream& outfile)
{
	if(iteration_counter >= 10 && sigma < 0.08 && sigma > 0)
	{
		totaltime.stopTimer();
		time = totaltime.getElapsedTimeInMilliSec();
		outfile << "Early termination for highly unreliable system." << endl
		<< "Iteration: " << iteration_counter << endl
		<< "R: " << reliable << endl
		<< "sigma: " << sigma << endl
		<< "Time: " << time << "ms" << endl; 
		
		
		return 0;
	}
	else if(iteration_counter >= 20000 && reliable > 0.999999)
	{
		totaltime.stopTimer();
		time = totaltime.getElapsedTimeInMilliSec();
		outfile << "Converged for highly reliable system." << endl
		<< setw(15) << "Iteration: " << iteration_counter << endl
		<< setw(15) << "R: " << reliable << endl
		<< setw(15) << "sigma: " << sigma << endl
		<< setw(15) << "Time: " << time << "ms" << endl;
		return 0;
	}		
	else
		return 1;
}

void memory_release(bool **sample)
{
	// sample[server][device]
	if(sample != NULL)
	{
		// deallocate each row
		for(int i = 0; i < server_num; i++)
			free(sample[i]);
		// deallocate array of pointers
		free(sample);
	}
}

// Define afri ,unit_num, and device_num
void find_AFR_DN(int device_case, double& afri, int& unit_num, int& device_num)
{
	switch(device_case)
	{
		case 0: afri = AFR_P;
				unit_num = UNIT_P;
				device_num = DEVICE_P;
				break;
		case 1: afri = AFR_M;
				unit_num = UNIT_M;
				device_num = DEVICE_M;
				break;
		case 2: afri = AFR_H;
				unit_num = UNIT_H;
				device_num = DEVICE_H;
				break;
		case 3: afri = AFR_B;
				unit_num = UNIT_B;
				device_num = DEVICE_B;
				break;
		default: cout << "Device number: 0 for P, 1 for M, 2 for H, 3 for B.\n";
	}

}

// double cal_avgDeviceUtili(const int& requested, const int& available, double& acumu_utili, const int& iteration_counter)
// {			
	// double utili = requested/(double)available;
	// acumu_utili += utili;			
	// return acumu_utili/iteration_counter;
// }

// if available = 0, device fail, utilization = 0
		// if utilization > 100%, device fail, utilization = 0
double cal_utili(const int& requested, const int& available)
{
	double utili;
	if(available == 0 || requested > available)
		utili = 0.0;
	else
		utili = requested * 1.0/available;
	
	return utili;
}
